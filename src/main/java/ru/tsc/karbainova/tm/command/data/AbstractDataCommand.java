package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.command.AbstractCommand;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;
import ru.tsc.karbainova.tm.exception.empty.EmptyDomainException;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NonNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";
    @NonNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";
    @NonNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";
    @NonNull
    protected static final String SYSTEM_JSON_PROPERTY_NAME = "javax.xml.bind.context.factory";
    @NonNull
    protected static final String SYSTEM_JSON_PROPERTY_VALUE = "org.eclipse.persistence.jaxb.JAXBContextFactory";
    @NonNull
    protected static final String JAXB_JSON_PROPERTY_NAME = "ecliselink.media-type";
    @NonNull
    protected static final String JAXB_JSON_PROPERTY_VALUE = "application/json";
    @NonNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";
    @NonNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";
    @NonNull
    protected static final String FILE_BINARY = "./data.bin";
    @NonNull
    protected static final String FILE_BINARY64 = "./data.base64";

    public abstract String name();

    public abstract String arg();

    public abstract String description();

    public abstract void execute();

    @NonNull
    public Domain getDomain() {
        @NonNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    public void setDomain(@Nullable Domain domain) {
        if (domain == null) throw new EmptyDomainException();

        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());

        serviceLocator.getAuthService().logout();
    }

    public @Nullable
    abstract Role[] roles();
}
