package ru.tsc.karbainova.tm.comparator;

import lombok.NonNull;
import ru.tsc.karbainova.tm.command.AbstractCommand;

import java.util.Comparator;

public class ComparatorCommand implements Comparator<Class<? extends AbstractCommand>> {

    @NonNull
    private static final ComparatorCommand instance = new ComparatorCommand();

    private ComparatorCommand() {
    }

    @NonNull
    public static ComparatorCommand getInstance() {
        return instance;
    }

    @Override
    public int compare(Class o1, Class o2) {
        if (o1.getSuperclass().getName().equals(o2.getSuperclass().getName()))
            return o1.getName().compareTo(o2.getName());
        return o1.getSuperclass().getName().compareTo(o2.getSuperclass().getName());
    }
}
